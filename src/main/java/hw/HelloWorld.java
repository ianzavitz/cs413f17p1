package hw;

public class HelloWorld {

  public String getMessage() {
    return "hello world";
  }

  public int getYear() {
    return 2009;
  }
  // I changed the return from 2008 to 2009
}
